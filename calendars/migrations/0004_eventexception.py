# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2020-12-04 22:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('calendars', '0003_auto_20200824_2049'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventException',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('old_start', models.DateTimeField()),
                ('new_start', models.DateTimeField(blank=True, null=True)),
                ('old_end', models.DateTimeField(blank=True, null=True)),
                ('new_end', models.DateTimeField(blank=True, null=True)),
                ('reason', models.CharField(blank=True, max_length=128, null=True)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exceptions', to='calendars.Event')),
            ],
        ),
    ]
