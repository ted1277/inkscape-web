# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2021-12-20 05:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forums', '0031_forumtopic_attachment_override'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='forumtopic',
            name='attachment_override',
        ),
        migrations.AddField(
            model_name='forumtopic',
            name='is_contest',
            field=models.BooleanField(default=False),
        ),
    ]
