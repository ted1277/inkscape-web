#
# Copyright 2021, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Automatically remove old users
"""

from datetime import timedelta

from django.core.management import BaseCommand
from django.utils.timezone import now

from person.models import User

class Command(BaseCommand):
    """Run expire unclaimed users"""
    help = __doc__

    def handle(self, *args, **options):
        ago = now() - timedelta(days=30)
        # Many checks to not delete good data
        for user in User.objects.filter(
            is_active=False,
            date_joined__lte=ago,
            resources__isnull=True,
            last_seen__isnull=True,
            photo__isnull=True,
            bio__isnull=True,
            ):
            print(" [DELETING] %s " % str(user))
            user.delete()
